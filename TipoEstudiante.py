from experta import *
from pprint import pprint
from estudiante import obtener_estudiantes
from openpyxl import load_workbook


class Repitencia( Fact ):
    pass


class TipoEstudiante( KnowledgeEngine ):
    def __init__(self):
        super().__init__()
        self.estudiantes = []
        self.index_actual_estudiante = 0

    @Rule( Repitencia( repitencia=MATCH.repitencia ), TEST( lambda repitencia: repitencia == 1) )
    def _verificar_regular(self):
        self.estudiantes[self.index_actual_estudiante][4] = "regular"
        self.reset()

    @Rule(Repitencia(repitencia=MATCH.repitencia), TEST(lambda repitencia: repitencia == 2))
    def _verificar_repetidor(self):
        self.estudiantes[self.index_actual_estudiante][4] = "repetidor"
        self.reset()

    @Rule(Repitencia(repitencia=MATCH.repitencia), TEST(lambda repitencia: repitencia == 3))
    def _verificar_oyente(self):
        self.estudiantes[self.index_actual_estudiante][4] = "oyente"
        self.reset()

    def asignar_tipo_estudiante(self):
        wb = load_workbook(filename=r'estudiantes.xlsx')
        sheet = wb.active
        tipo = TipoEstudiante()
        tipo.reset()
        tipo.estudiantes = obtener_estudiantes()
        for index, estudiante in enumerate(tipo.estudiantes):
            tipo.index_actual_estudiante = index
            tipo.declare( Repitencia( repitencia=estudiante[3] ) )
            tipo.run()
            sheet[f"E{index+2}"] = tipo.estudiantes[index][4]
        wb.save(filename=r'estudiantes.xlsx')
        return tipo.estudiantes


if __name__ == "__main__":
    pprint(TipoEstudiante().asignar_tipo_estudiante())

