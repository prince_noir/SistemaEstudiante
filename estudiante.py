
from openpyxl import load_workbook
from pprint import pprint
from math import sqrt

def obtener_estudiantes():
    estudiantes = []
    data = []
    wb = load_workbook(filename=r'estudiantes.xlsx')
    sheet = wb.active
    for row in sheet.iter_rows(min_row=2, values_only=True):
        for cell in row: data.append(cell)
        estudiantes.append(data)
        data = []
    return estudiantes


def agregar_estudiante(estudiante):
    repitencias = []
    wb = load_workbook(filename=r'estudiantes.xlsx')
    sheet = wb.active
    for row in sheet[f"D2:D{sheet.max_row}"]:
        for cell in row: repitencias.append(cell.value)
    distancias = [sqrt(abs(repitencia - estudiante[2])**2) for repitencia in repitencias]
    estudiante[2] = min(distancias)
    estudiante.insert(0, sheet.max_row)
    sheet.append(estudiante)
    wb.save(filename=r'estudiantes.xlsx')



if __name__ == "__main__":
    pprint(obtener_estudiantes())

