from openpyxl.styles import Alignment, PatternFill
from openpyxl import load_workbook


def align(position="center"):
    wb = load_workbook(filename=r'estudiantes.xlsx')
    sheet = wb.active
    for row in sheet[f"A2:E{sheet.max_row}"]:
        for cell in row:
            cell.alignment = Alignment(horizontal=position)
    wb.save(filename=r'estudiantes.xlsx')

